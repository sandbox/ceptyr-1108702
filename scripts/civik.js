var sidebarMoving = false; 
var sidebarShown = false;

function sidebar_full()
{
	if(!sidebarMoving)
	{
		sidebarMoving = true;
		$("#sidebar-first").animate({"left": "-40px"}, 250, function(){
			sidebarMoving = false;
			sidebarShown = true;
		});
	}
}

function sidebar_mini()
{
	if(!sidebarMoving)
	{
		sidebarMoving = true;
		$("#sidebar-first").animate({"left": "-240px"}, 300,function(){
			sidebarMoving = false;
			sidebarShown = false;
		});
	}
}

function sidebar_none()
{
	if(!sidebarMoving)
	{
		sidebarMoving = true;
		$("#sidebar-first").animate({"left": "-260px"}, 250,function(){
			sidebarMoving = false;
			sidebarShown = false;
		});
	}
}



$(document).ready(function(){ 
	
	// Hide the sidebar
	sidebar_mini();			
	
	$("#sidebar-first").bind("mouseenter", sidebar_check);
//	$("#sidebar-first").bind("mouseleave", sidebar_mini);
	
	
	$(document).bind("mousemove", sidebar_check);
});

function sidebar_check(e)
{
		// Mouse positions
		var posX = e.pageX;
		var posY = e.pageY;
		
		// Screen resolution
		var width = $(document).width();
		var height = $(document).height();
		
		// Note, we do not animate if there is another animation in progress
		var screenPositionRatio = posX / width;
		
		// Show the sidebar if we're in the left 40px
		if(posX <= 40 && !sidebarShown)
		{
			sidebar_full();
		}
		// Show the mini sidebar if we're in the left 255px, or the left third of the screen
		else if((posX <= 255 || screenPositionRatio <= 0.33) && !sidebarShown)
		{
			sidebar_mini();
		}
		// Hide the side bar if we're not in the left third of the screen
		else if((posX > 255 || screenPositionRatio > 0.33) && !sidebarShown)
		{
			sidebar_none();
		}
		// Mini side bar if we've moved our mouse away from the left hand column but we're still in the left third of the screen
		else if(posX > 255 && screenPositionRatio <= 0.33)
		{
			sidebar_mini();
		}
		// Hide the side bar if we've moved our mouse away into the right two thirds of the screen
		else if(posX > 255 && screenPositionRatio > 0.33)
		{
			sidebar_none();
		}
}

