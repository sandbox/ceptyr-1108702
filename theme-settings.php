<?php

//contributor_logo

function civik_settings($saved_settings) 
{
  $settings = theme_get_settings('civik');
  
  $form['logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contributor Logo'),
    '#description' => t("Displayed on Civik Menu")
  );
  $form['logo']['contributor_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Contributor Name'),
    '#default_value' => $settings['contributor_name'],
  );
  $form['logo']['contributor_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Contributor URL'),
    '#default_value' => $settings['contributor_url'],
  );
  $form['logo']['contributor_logo_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Contributor Logo'),
    '#default_value' => $settings['contributor_logo_path'],
  );
  $form['logo']['contributor_logo_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload a Contributor Logo Image'),
    '#description' => t('Your image will be scaled to a maximum of 225px wide. If your image is smaller than 225px wide, it will retain its existing dimensions')
  );
  
  $form['#submit'][] = 'civik_settings_submit';
  $form['logo']['logo_upload']['#element_validate'][] = 'civik_settings_submit';

  return $form;
}

function civik_settings_submit($form, &$form_state) 
{
  // Check for a new uploaded file, and use that if available.
  if ($file = file_save_upload('contributor_logo_upload')) 
  {
    $parts = pathinfo($file->filename);
    $filename = (! empty($key)) ? 
                                str_replace('/', '_', $key) .'_logo.'. $parts['extension'] : 
                                'civik_logo.'. $parts['extension'];

//    dsm(array("file" => $file, "key" => $key, "parts" => $parts, "filename" => $filename));
    
    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename)) {
      $_POST['contributor_logo_path'] = $form_state['values']['contributor_logo_path'] = $file->filepath;
    }
  }
}