<?php

/**
 * hook_breadcrumb($crumbs) override
 * 
 * This function removes extraneous breadcrumb references
 */
function civik_breadcrumb($breadcrumb)
{
  $output = '';

  $theme_settings = theme_get_settings("civik");
  $show_breadcrumb_bar = true;
  $show_title_in_breadcrumb = true;
  $show_title_as_link = false;

  // Sanity escape
  if(!$show_breadcrumb_bar) { return ""; }
    
  if($show_title_in_breadcrumb)
  {
    // Add current page onto the end.
    if (!drupal_is_front_page()) {
      $item = drupal_get_title();
      
      if($show_title_as_link) 
      {
        $path_alias = drupal_get_path_alias(request_uri());
        $item = "<a href='".$path_alias."' title='".check_plain($item)."'>".check_plain($item)."</a>";
      }
      else
      {
        $item = "<strong>". check_plain($item) ."</strong>";
      }
      
      $breadcrumb[] = $item;
    }
  }
  
  // Remove the home link.
  foreach ($breadcrumb as $key => $link) {
    if (strip_tags($link) === t('Home')) {
      unset($breadcrumb[$key]);
      break;
    }
  }

  foreach ($breadcrumb as $link) {
    $output .= "<span class='breadcrumb-link'>{$link}</span>";
  }
  return $output;
}

/**
 * hook_preprocess_page(&$vars) override
 *
 * This function finds the width of the specified contributor logo path 
 * and proceeds to scale the image based on the image aspect ratio, so that
 * the image will properly fit at the bottom of the Civik Menu
 */
function civik_preprocess_page(&$vars)
{
  $theme_info = theme_get_settings("civik");
  $logo_info = image_get_info($theme_info["contributor_logo_path"]);
  
  if(is_array($logo_info))
  {
    $width = $logo_info["width"];
    $height = $logo_info["height"];

    if($width > 225)
    {
        $aspect_ratio = $height / $width;
        $width = 225;
        $height = $width * $aspect_ratio;
    }
  
    $vars["contributor_logo"] = array(
      "width" => $width,
      "height" => $height,
      "path" => base_path().$theme_info["contributor_logo_path"]
    );
  }
}